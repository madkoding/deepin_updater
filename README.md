# Deepin post install script
#### For programmers and first deepin timers

This post install script updates `sources.list` with faster mirror
than China one (replaced with Brazil deepin mirror) and
download a few packages for some web developers.

This is the post install list that you can manage selecting them in the `config.cfg` file

* `Infinality` font smoothing (enable OSX by default hint)
* Install `hack` monospaced font
* Install `mongoDB` server
* Install `curl`
* Install `git` and `git flow`
* Install `NVM` and node packages
  * Lastest stable `node.js` version
  * Strongloop Loopback
  * `gulp`, `bower`and updated `npm`
* Install `composer` and `laravel`
* Install Google Talk plugin
* Install `VirtualBox` and `Vagrant` for virtual machines

### Instalation and use

Clone this repo:
```
git clone https://gitlab.com/madkoding/deepin_updater.git
```

Enter to the deepin_updater cloned folder, give permissions and execute:
```
cd deepin_updater
chmod +x deepin_updater.sh
sh deepin_updater.sh
```

*** Remember to edit the `config.cfg` file to see what to install ***
