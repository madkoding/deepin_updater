#!/bin/bash

# Read config file from config.cfg
echo "Reading config...." >&2
. ./config.cfg

# Update grub to not freeze dell
if $FIX_GRUB_DELL
then
    sudo sed -i 's#^\(GRUB_CMDLINE_LINUX_DEFAULT="splash quiet\)"$#\1 intel_idle.max_cstate=1 i915.modeset=1 i915.lvds_downclock=1"#' /etc/default/grub
    sudo update-grub
fi

# To set max number of inotify watches (fix watch max files problems)
if $FIX_INOTIFY_WATCHES
then
    echo 524288 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
fi

# Create new sources.list for Brazil deepin mirror
if $FIX_DEEPIN_MIRROR
then
    sudo rm /etc/apt/sources.list
    touch /etc/apt/sources.list
    echo "# Deepin mirror for sources.list:" | sudo tee /etc/apt/sources.list
    echo "deb [by-hash=force] http://www.gtlib.gatech.edu/pub/deepin/ unstable main contrib non-free" | sudo tee -a /etc/apt/sources.list
fi

# Install MongoDB 3.2
if $INSTALL_MONGODB
then
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
fi

# Add font smoothing
if $INSTALL_INFINALITY
then
    echo "# Infinality sources" | sudo tee -a /etc/apt/sources.list.d/infinality.list
    echo "deb http://ppa.launchpad.net/no1wantdthisname/ppa/ubuntu trusty main" | sudo tee /etc/apt/sources.list.d/infinality.list
    echo "deb-src http://ppa.launchpad.net/no1wantdthisname/ppa/ubuntu trusty main" | sudo tee -a /etc/apt/sources.list.d/infinality.list
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E985B27B
fi

# Add virtualbox sources
if $INSTALL_VIRTUALBOX
then
    echo "# Virtualbox sources" | sudo tee /etc/apt/sources.list.d/virtualbox.list
    echo "deb http://download.virtualbox.org/virtualbox/debian vivid contrib" | sudo tee -a /etc/apt/sources.list.d/virtualbox.list
    wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
fi

# Basic update
sudo apt-get -y update
sudo apt-get -y upgrade

# Install git (just in case ;)
sudo apt-get -y install git

# Install curl
sudo apt-get -y install curl

# Install zip
sudo apt-get -y install zip unzip

# Install gitflow
if $INSTALL_GITFLOW
then
    git clone https://github.com/datasift/gitflow
    cd gitflow
    sudo ./install.sh
    cd ..
    sudo rm -rf gitflow
fi

# Install Virtualbox
if $INSTALL_VIRTUALBOX
then
    sudo apt-get -y install dkms
    sudo apt-get -y install virtualbox-5.0
fi

# Install Virtuabox Extensions Pack
if $INSTALL_VIRTUALBOX
then
    wget -q http://download.virtualbox.org/virtualbox/5.0.18/Oracle_VM_VirtualBox_Extension_Pack-5.0.18-106667.vbox-extpack
    VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-5.0.18-106667.vbox-extpack
    rm Oracle_VM_VirtualBox_Extension_Pack-5.0.18-106667.vbox-extpack
fi

# Google talk plugin
if $INSTALL_GOOGLE_TALK
then
    sudo touch /etc/default/google-talkplugin
    ARCH=$( uname -m )
    if [ $ARCH == "X86_64" ]
    then
        wget https://dl.google.com/linux/direct/google-talkplugin_current_amd64.deb && wget https://dl.google.com/linux/direct/google-talkplugin_current_amd64.deb && sudo dpkg -i google-talkplugin_current_amd64.deb && rm google-talkplugin_current_amd64.deb
    else
        wget https://dl.google.com/linux/direct/google-talkplugin_current_i386.deb && wget https://dl.google.com/linux/direct/google-talkplugin_current_i386.deb && sudo dpkg -i google-talkplugin_current_i386.deb && rm google-talkplugin_current_i386.deb
    fi
fi

# Configure font smoothing after reboot
if $INSTALL_INFINALITY
then
    sudo apt-get -y install fontconfig-infinality
    sudo bash /etc/fonts/infinality/infctl.sh setstyle osx
fi

# Install Hack fonts (better for programming)
sudo apt-get -y install fonts-hack-ttf

# Install mongo database
if $INSTALL_MONGODB
then
    # sudo apt-get -y install mongodb-org
    sudo apt-get install -y mongodb-org=3.2.6 mongodb-org-server=3.2.6 mongodb-org-shell=3.2.6 mongodb-org-mongos=3.2.6 mongodb-org-tools=3.2.6
fi

# Install Curl and NVM
if $INSTALL_NVM
then
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
    nvm install 4 # Install Node 4.x.x
    nvm use 4
    npm install -g npm # Update npm in case of any other update come
    npm install -g bower
    npm install -g gulp
    echo "# Starts NVM on terminal open" | sudo tee -a ~/.bashrc
    echo "nvm use 4" | sudo tee -a ~/.bashrc
fi

# Install strongloop loopback
if $INSTALL_LOOPBACK
then
    npm install -g strongloop
    npm install -g nodemon
fi

## TODO : Composer and laravel needs to be fixed

# install Composer
if $INSTALL_COMPOSER
then
    sudo apt-get -y install php5-odbc php5-pgsql php5-curl php5-gd php5-xdebug apache2 php5-cli
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
    sudo chmod 755 /usr/local/bin/composer
fi

# install Laravel for user
if $INSTALL_LARAVEL
then
    composer global require "laravel/installer=~1.3.1"
    echo 'PATH="$PATH:~/.composer/vendor/bin"' | tee -a ~/.profile
    composer global update
fi

# Update WPS dictionary with spanish language
cd /opt/kingsoft/wps-office/office6/dicts
sudo wget http://wps-community.org/download/dicts/es_ES.zip
sudo unzip es_ES.zip
cd ~

# Update WPS with CHILE dic
cd /opt/kingsoft/wps-office/office6/dicts
sudo wget http://ftp.rediris.es/ftp/mirror/openoffice.org/contrib/dictionaries/es_CL.zip
sudo unzip es_CL.zip
sudo mv main.aff main.aff.old
sudo mv main.dic main.dic.old
sudo mv es_CL.aff main.aff
sudo mv es_CL.dic main.dic
cd ~

# Install Vagrant
if ($INSTALL_VAGRANT && $INSTALL_VIRTUALBOX)
then
    sudo apt-get -y install vagrant
    sudo apt-get -y install build-essential patch
    sudo apt-get -y install ruby-dev zlib1g-dev liblzma-dev
    sudo gem install nokogiri
    vagrant plugin install vagrant-bindfs
fi
# Recomended vagrant: https://github.com/markdunphy/node-mongo-vagrant.git


# prompt for a reboot
clear
echo ""
echo "==================="
echo " TIME FOR A REBOOT!"
echo "==================="
echo ""
